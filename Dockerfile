FROM node:20
CMD node ./server.js
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
RUN npm i --production
COPY ./local.js ./
COPY ./index.js ./
COPY ./server.js ./
COPY ./libs ./libs
