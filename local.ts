const config = {
  dbFile: process.env.FILE_DB || "/tmp/db.json",
  port: process.env.PORT ? Number(process.env.PORT) : 3000,
  jwt: {
    secret: process.env.JWT_SECRET || "bà67o98bo_87tv6or8v6ro8",
    config: Object.freeze({
      expiresIn: process.env.JWT_TOKENEXPIRESINSECONDS
        ? Number(process.env.JWT_TOKENEXPIRESINSECONDS)
        : 60 * 60 * 24,
      issuer: process.env.JWT_ISSUER || "test",
      audience: process.env.JWT_AUDIENCE || "test",
    }),
  },
  adminPassword: process.env.ADMINPASSWORD || "admin",
};

export default config;
