import bcrypt from "bcrypt";
export async function generatePassword(password) {
    const hash = await bcrypt.hash(password, 10);
    return hash;
}
export async function comparePassword(password, hash) {
    const match = await bcrypt.compare(password, hash);
    return match;
}
//# sourceMappingURL=users.js.map