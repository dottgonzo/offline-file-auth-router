import { Low } from "lowdb";
import { JSONFile } from "lowdb/node";
import lodash from "lodash";
import { generatePassword } from "./users.js";
class LowWithLodash extends Low {
    constructor() {
        super(...arguments);
        this.chain = lodash.chain(this).get("data");
    }
}
const defaultData = {
    users: [],
};
export async function initializeDb(dbFile, opts) {
    const adapter = new JSONFile(dbFile);
    const db = new LowWithLodash(adapter, defaultData);
    await db.read();
    const users = db.chain.get("users");
    const admin = users.find({}).value();
    if (!admin?._id) {
        const password = await generatePassword(opts?.adminPassword || "admin");
        db.data.users.push({
            _id: "1",
            name: "admin",
            role: "admin",
            email: "admin@localhost",
            password,
            status: "active",
        });
        await db.write();
    }
    return db;
}
//# sourceMappingURL=db.js.map