import { Low } from "lowdb";
import { JSONFile } from "lowdb/node";
import lodash from "lodash";
import { generatePassword } from "./users.js";
class LowWithLodash<T> extends Low<T> {
  chain: lodash.ExpChain<this["data"]> = lodash.chain(this).get("data");
}
export type User = {
  _id: string;
  role: "user" | "admin";
  name: string;
  email: string;
  password: string;
  status: "active" | "inactive";
};

type Data = {
  users: User[];
};

const defaultData: Data = {
  users: [],
};

export async function initializeDb(
  dbFile: string,
  opts?: { adminPassword?: string }
) {
  const adapter = new JSONFile<Data>(dbFile);
  const db = new LowWithLodash(adapter, defaultData);

  await db.read();
  const users = db.chain.get("users");
  const admin = users.find({}).value();
  if (!admin?._id) {
    const password = await generatePassword(opts?.adminPassword || "admin");
    db.data.users.push({
      _id: "1",
      name: "admin",
      role: "admin",
      email: "admin@localhost",
      password,
      status: "active",
    });
    await db.write();
  }

  return db;
}
