import express from "express";
import getExpressAuthRouter from "./index.js";
import config from "./local.js";
getExpressAuthRouter({
    dbFile: config.dbFile,
    jwt: config.jwt,
    adminPassword: config.adminPassword,
})
    .then((authRouter) => {
    const app = express();
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());
    app.use("/auth", authRouter);
    app.use("/ping", (req, res) => {
        res.send("pong");
    });
    app.listen(config.port, "0.0.0.0", () => {
        console.log(`Listening on port ${config.port}`);
    });
})
    .catch((err) => {
    console.error(err);
    process.exit(1);
});
//# sourceMappingURL=server.js.map