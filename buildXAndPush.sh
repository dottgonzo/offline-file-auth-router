#!/bin/bash

VERSION=$(cat package.json | grep '"version"' | head -1|sed 's/ //g'|sed 's/"//g'|sed 's/:/ /g'|sed 's/,//' | awk '{print($2)}')
REGISTRY_URI=$(cat package.json | grep '"registry_uri"' | head -1|sed 's/ //g'|sed 's/"//g'|sed 's/:/ /g'|sed 's/,//' | awk '{print($2)}')

echo building $REGISTRY_URI with version: \"master\" and \"$VERSION\"

docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t $REGISTRY_URI:master -t $REGISTRY_URI:$VERSION --push .
