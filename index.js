import { initializeDb } from "./libs/db.js";
import * as getAuthRouter from "simple-auth-passport-router-lib/index.js";
import { comparePassword } from "./libs/users.js";
export default async function getExpressAuthRouter(opts) {
    const db = await initializeDb(opts.dbFile, {
        adminPassword: opts?.adminPassword,
    });
    const users = db.chain.get("users");
    // console.log(users.find({}).value())
    const routerEvents = {
        check_local_user_credentials: async (email, password) => {
            const user = users.find({ email }).value();
            if (!user)
                throw new Error("User not found");
            const match = await comparePassword(password, user.password);
            if (!match)
                throw new Error("Password does not match");
            return {
                _id: user._id,
                role: user.role,
                status: user.status,
                email: user.email,
                member_id: user._id,
            };
        },
        get_user_by_email: async (email) => {
            return users.find({ email }).value();
        },
    };
    const authRouter = getAuthRouter.default.default(routerEvents, {
        jwt: opts.jwt.config,
        jwtSecret: opts.jwt.secret,
        gmailSmtpServerAuth: null,
        socialRegistration: null,
        socialLogin: null,
        serverUri: null,
        local: {},
    });
    return authRouter;
}
//# sourceMappingURL=index.js.map